package isp.luntrasu.sergiu.Examen.Subiect2;

import isp.luntrasu.sergiu.Sumare.Testare2;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

public class Sub2 implements ActionListener {
    JFrame frame;
    JButton button;
    JPanel panel;
    JTextField t;

    public Sub2(){
        frame = new JFrame();
        t = new JTextField();
        t.setSize(200,200);
        t.setVisible(true);
        button = new JButton("Generate number");
        button.addActionListener(this);
        panel = new JPanel();
        panel.setBorder(BorderFactory.createEmptyBorder(30, 30, 30, 30));
        panel.setLayout(new GridLayout(0, 1));
        panel.add(t);
        panel.add(button);
        frame.add(panel, BorderLayout.CENTER);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        new Sub2();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
            Random rand =new Random();
            int g = rand.nextInt(50);
            t.setText(g + "\n");
    }
}
